

var mysql = require('mysql');

var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database: "mydb"
  });
   
connection.connect(function(err) {
    if (err) {
        console.log('err, ',  err);
        return;
    }
    console.log("Connected!");
    connection.query("CREATE DATABASE IF NOT EXISTS mydb", function (err, result) {
        if (err) throw err;
        console.log("Database created");
    });
    var sqlCrateTalbe = "CREATE TABLE IF NOT EXISTS data (name VARCHAR(255), email VARCHAR(255),  phonenumber VARCHAR(255))";
    connection.query(sqlCrateTalbe, function (err, result) {
        if (err) throw err;
        console.log("Table created");
    });
});

module.exports = connection;
