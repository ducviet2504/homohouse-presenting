var express = require('express');
var router = express.Router();
var fs = require('fs');
var con = require('../database');
var firebase = require('firebase/app');
var firebaseDatabase = require('firebase/database');

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render("index", { title: 'Express' });
});

router.post('/', function (req, res, next) {
  // const dataFileName = "data.txt";
  const name = req.body.name;
  const phone = req.body.phone;
  const email = req.body.email;
  const value = req.body.couter;
  const value_no = req.body.couter_no;

  if(value_no) {
    updates= {}
    updates['counter/counter_no/value'] = value_no;
    firebase.database().ref().update(updates);
  } else {

    var database = firebase.database();
    database.ref('users/').push({
      name: name,
      email: email,
      phone: phone,
    });
    updates= {}
    updates['counter/counter_submit/value'] = value;
     firebase.database().ref().update(updates);
  }
  
  res.render('index', { title: 'Express' });
});
module.exports = router;
