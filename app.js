var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var firebase = require('firebase/app');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

var firebaseConfig = {
  apiKey: "AIzaSyDTgmSV73kMypfn1TUwLUwLnPLqpQS3HSo",
  authDomain: "homohouse.firebaseapp.com",
  databaseURL: "https://homohouse.firebaseio.com",
  projectId: "homohouse",
  storageBucket: "homohouse.appspot.com",
  messagingSenderId: "89530089133",
  appId: "1:89530089133:web:2e8a152c4857466b3e5024",
  measurementId: "G-V6DWPEF55W"
};
firebase.initializeApp(firebaseConfig);

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser()); 
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
module.exports = app;
